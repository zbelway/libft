/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 18:59:10 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/25 19:25:55 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void		ft_strrev(char *str)
{
	char	*rts;

	rts = str;
	while (*rts)
		rts++;
	rts--;
	while (str < rts)
	{
		*str ^= *rts;
		*rts ^= *str;
		*str++ ^= *rts--;
	}
}
